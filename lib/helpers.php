<?php
/**
 * Helper functions
 *
 * @package         Woo_Byo_Pcs
 */

 function BYO_GetSoapResult( $string, &$error) {

   $xml = simplexml_load_string( $string, NULL, NULL, "http://schemas.xmlsoap.org/soap/envelope/");
   $ns = $xml->getNamespaces( true );
   $soap = $xml->children( $ns['soap'] );
   $res = $soap->Body->children();
   // var_dump ( $res );
   // print_r( $res);
   // var_dump( $res->NewOrderValidateResponse);
   $xml = simplexml_load_string( $res->NewOrderValidateResponse->NewOrderValidateResult);
   // var_dump( $xml);
   // var_dump( $xml->Success);
   // echo " sucesss: ".$xml->Success;
   $result = ( $xml->Success == 1) ? true : false;

   //var_dump( $xml );

   if ( !$result) {
     $error = $xml->Errors->Error[ 1]->ErrMessage;
   }
   else {
     $error = '';
   }

   return( $result);
 }


function BYO_SetData( &$headers, &$body, $pcs, $customer_data, $product_data ) {

  $headers = array(
     'Host' => $pcs[ 'Host'],
     'Content-Type' => 'text/xml; charset=utf-8',
     'SOAPAction' => $pcs[ 'SOAPActionNewOrder'],
     );

  $soap_header = '';
  $soap_header .= '<soap:Header>';
  $soap_header .= '<NewOrderAuthenticationHeader xmlns="'.$pcs[ 'xmlns'] .'">';
  $soap_header .= '<UserID>'.$pcs[ 'UserID'].'</UserID>';
  $soap_header .= '<Password>'.$pcs[ 'Password'].'</Password>';
  $soap_header .= '</NewOrderAuthenticationHeader>';
  $soap_header .= '</soap:Header>';

  $soap_body = '';
  $soap_body .= '<soap:Body>';
  $soap_body .= '<NewOrderValidate xmlns="'.$pcs[ 'xmlns'].'">';
  $soap_body .= '<PUB>'.$pcs[ 'PUB'].'</PUB>';
  $soap_body .= '<FNAME>'.$customer_data[ 'shipping_first_name'].'</FNAME>';
  $soap_body .= '<LNAME>'.$customer_data[ 'shipping_last_name'].'</LNAME>';
  $soap_body .= '<PREFIX></PREFIX>';
  $soap_body .= '<SUFFIX></SUFFIX>';
  $soap_body .= '<TITLE></TITLE>';
  $soap_body .= '<DEPT></DEPT>';
  $soap_body .= '<COMPANY></COMPANY>';
  $soap_body .= '<STREET1>'.$customer_data[ 'shipping_address_1'].'</STREET1>';
  $soap_body .= '<STREET2>'.$customer_data[ 'shipping_address_2'].'</STREET2>';
  $soap_body .= '<CITY>'.$customer_data[ 'shipping_city'].'</CITY>';
  $soap_body .= '<STATE>'.$customer_data[ 'shipping_state'].'</STATE>';
  $soap_body .= '<ZIP>'.$customer_data[ 'shipping_postcode'].'</ZIP>';
  $soap_body .= '<COUNTRYCODE>'.$customer_data[ 'shipping_country'].'</COUNTRYCODE>';
  $soap_body .= '<RENTPOSTAL></RENTPOSTAL>';
  $soap_body .= '<RENTEMAIL></RENTEMAIL>';
  $soap_body .= '<RENTPHONE></RENTPHONE>';
  $soap_body .= '<RENTFAX></RENTFAX>';
  $soap_body .= '<PHONE></PHONE>';
  $soap_body .= '<FAX></FAX>';
  $soap_body .= '<EMAIL></EMAIL>';
  $soap_body .= '<TRACK>'.$pcs[ 'TRACK'].'</TRACK>';
  $soap_body .= '<TERM>8</TERM>';
  $soap_body .= '<COPIES>1</COPIES>';
  $soap_body .= '<PREMIUM></PREMIUM>';
  $soap_body .= '<SERVICE></SERVICE>';
  $soap_body .= '<PRICE>'.$product_data[ 'price'].'</PRICE>';
  $soap_body .= '<TAX>0.00</TAX>';
  $soap_body .= '<PAID>'.$product_data[ 'price'].'</PAID>';
  $soap_body .= '<PAYTYPE>O</PAYTYPE>';
  $soap_body .= '<RENSER></RENSER>';
  $soap_body .= '<CC_TX_ID></CC_TX_ID>';
  $soap_body .= '<CC_TOKEN></CC_TOKEN>';
  $soap_body .= '<CLIENTORDERID></CLIENTORDERID>';
  $soap_body .= '<VALIDATION_ONLY>1</VALIDATION_ONLY>';
  $soap_body .= '<DONEE_FNAME></DONEE_FNAME>';
  $soap_body .= '<DONEE_LNAME></DONEE_LNAME>';
  $soap_body .= '<DONEE_PREFIX></DONEE_PREFIX>';
  $soap_body .= '<DONEE_SUFFIX></DONEE_SUFFIX>';
  $soap_body .= '<DONEE_TITLE></DONEE_TITLE>';
  $soap_body .= '<DONEE_DEPT></DONEE_DEPT>';
  $soap_body .= '<DONEE_COMPANY></DONEE_COMPANY>';
  $soap_body .= '<DONEE_STREET1></DONEE_STREET1>';
  $soap_body .= '<DONEE_STREET2></DONEE_STREET2>';
  $soap_body .= '<DONEE_CITY></DONEE_CITY>';
  $soap_body .= '<DONEE_STATE></DONEE_STATE>';
  $soap_body .= '<DONEE_ZIP></DONEE_ZIP>';
  $soap_body .= '<DONEE_COUNTRYCODE></DONEE_COUNTRYCODE>';
  $soap_body .= '<DONEE_PHONE></DONEE_PHONE>';
  $soap_body .= '<DONEE_FAX></DONEE_FAX>';
  $soap_body .= '<DONEE_EMAIL></DONEE_EMAIL>';
  $soap_body .= '<CREATE_PROFILE>0</CREATE_PROFILE>';
  $soap_body .= '<USERNAME></USERNAME>';
  $soap_body .= '<PASSWORD></PASSWORD>';
  $soap_body .= '<EXT_CUST_ID></EXT_CUST_ID>';
  $soap_body .= '<DONEE_EXT_CUST_ID></DONEE_EXT_CUST_ID>';
  $soap_body .= '</NewOrderValidate>';
  $soap_body .= '</soap:Body>';

  // build body
  $body = '<?xml version="1.0" encoding="utf-8"?>';
  $body .= '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
  $body .= $soap_header;
  $body .= $soap_body;
  $body .= '</soap:Envelope>';

  // remove white space from XML
  $body = str_replace( array( '\n', '\r', '\t'), '', $body);

}


// $url = 'https://subscribe.pcspublink.com/WSTransactions_v1_1_9/WSTransactions_v1_1_9.asmx';
//
// $pcs = array();
// $pcs[ 'URL'] = 'https://subscribe.pcspublink.com/WSTransactions_v1_1_9/WSTransactions_v1_1_9.asmx';
// $pcs[ 'Host'] = 'subscribe.pcspublink.com';
// $pcs[ 'SOAPActionNewOrder'] = '"https://subscribe.pcspublink.com/WSTransactions_v1_1_9/WSTransactions_v1_1_9.asmx/NewOrderValidate"';
//
// $pcs[ 'UserID'] = 'BREW_SWG#%*';
// $pcs[ 'Password'] = '0xF1968C02C753EF19A9606FA5B702C98B';
// $pcs[ 'xmlns'] = 'https://subscribe.pcspublink.com/WSTransactions_v1_1_9/WSTransactions_v1_1_9.asmx';
// $pcs[ 'PUB'] = 'BREW';
// $pcs[ 'TRACK'] = '9SW1F'; // new order
//
// $headers = array(
//    'Host' => $pcs[ 'Host'],
//    'Content-Type' => 'text/xml; charset=utf-8',
//    'SOAPAction' => $pcs[ 'SOAPActionNewOrder'],
//    );
//
//
// $source_code = '';
// $soap_header = '';
// $soap_header .= '<soap:Header>';
// $soap_header .= '<NewOrderAuthenticationHeader xmlns="'.$pcs[ 'xmlns'] .'">';
// $soap_header .= '<UserID>'.$pcs[ 'UserID'].'</UserID>';
// $soap_header .= '<Password>'.$pcs[ 'Password'].'</Password>';
// $soap_header .= '</NewOrderAuthenticationHeader>';
// $soap_header .= '</soap:Header>';
//
// $customer_data = array();
// $customer_data[ 'shipping_first_name'] = 'John';
// $customer_data[ 'shipping_last_name'] = 'Tester';
// $customer_data[ 'shipping_address_1'] = '1 Market Street';
// $customer_data[ 'shipping_address_2'] = '';
// $customer_data[ 'shipping_city'] = 'Portsmouth';
// $customer_data[ 'shipping_state'] = 'NH';
// $customer_data[ 'shipping_postcode'] = '03801';
// $customer_data[ 'shipping_country'] = 'USA';
//
// $order_data = array();
// $order_data[ 'price'] = '29.99';
//
// $soap_body = '';
// $soap_body .= '<soap:Body>';
// $soap_body .= '<NewOrderValidate xmlns="'.$pcs[ 'xmlns'].'">';
// $soap_body .= '<PUB>'.$pcs[ 'PUB'].'</PUB>';
// $soap_body .= '<FNAME>'.$customer_data[ 'shipping_first_name'].'</FNAME>';
// $soap_body .= '<LNAME>'.$customer_data[ 'shipping_last_name'].'</LNAME>';
// $soap_body .= '<PREFIX></PREFIX>';
// $soap_body .= '<SUFFIX></SUFFIX>';
// $soap_body .= '<TITLE></TITLE>';
// $soap_body .= '<DEPT></DEPT>';
// $soap_body .= '<COMPANY></COMPANY>';
// $soap_body .= '<STREET1>'.$customer_data[ 'shipping_address_1'].'</STREET1>';
// $soap_body .= '<STREET2>'.$customer_data[ 'shipping_address_2'].'</STREET2>';
// $soap_body .= '<CITY>'.$customer_data[ 'shipping_city'].'</CITY>';
// $soap_body .= '<STATE>'.$customer_data[ 'shipping_state'].'</STATE>';
// $soap_body .= '<ZIP>'.$customer_data[ 'shipping_postcode'].'</ZIP>';
// $soap_body .= '<COUNTRYCODE>'.$customer_data[ 'shipping_country'].'</COUNTRYCODE>';
// $soap_body .= '<RENTPOSTAL></RENTPOSTAL>';
// $soap_body .= '<RENTEMAIL></RENTEMAIL>';
// $soap_body .= '<RENTPHONE></RENTPHONE>';
// $soap_body .= '<RENTFAX></RENTFAX>';
// $soap_body .= '<PHONE></PHONE>';
// $soap_body .= '<FAX></FAX>';
// $soap_body .= '<EMAIL></EMAIL>';
// $soap_body .= '<TRACK>'.$pcs[ 'TRACK'].'</TRACK>';
// $soap_body .= '<TERM>8</TERM>';
// $soap_body .= '<COPIES>1</COPIES>';
// $soap_body .= '<PREMIUM></PREMIUM>';
// $soap_body .= '<SERVICE></SERVICE>';
// $soap_body .= '<PRICE>'.$order_data[ 'price'].'</PRICE>';
// $soap_body .= '<TAX>0.00</TAX>';
// $soap_body .= '<PAID>'.$order_data[ 'price'].'</PAID>';
// $soap_body .= '<PAYTYPE>O</PAYTYPE>';
// $soap_body .= '<RENSER></RENSER>';
// $soap_body .= '<CC_TX_ID></CC_TX_ID>';
// $soap_body .= '<CC_TOKEN></CC_TOKEN>';
// $soap_body .= '<CLIENTORDERID></CLIENTORDERID>';
// $soap_body .= '<VALIDATION_ONLY>1</VALIDATION_ONLY>';
// $soap_body .= '<DONEE_FNAME></DONEE_FNAME>';
// $soap_body .= '<DONEE_LNAME></DONEE_LNAME>';
// $soap_body .= '<DONEE_PREFIX></DONEE_PREFIX>';
// $soap_body .= '<DONEE_SUFFIX></DONEE_SUFFIX>';
// $soap_body .= '<DONEE_TITLE></DONEE_TITLE>';
// $soap_body .= '<DONEE_DEPT></DONEE_DEPT>';
// $soap_body .= '<DONEE_COMPANY></DONEE_COMPANY>';
// $soap_body .= '<DONEE_STREET1></DONEE_STREET1>';
// $soap_body .= '<DONEE_STREET2></DONEE_STREET2>';
// $soap_body .= '<DONEE_CITY></DONEE_CITY>';
// $soap_body .= '<DONEE_STATE></DONEE_STATE>';
// $soap_body .= '<DONEE_ZIP></DONEE_ZIP>';
// $soap_body .= '<DONEE_COUNTRYCODE></DONEE_COUNTRYCODE>';
// $soap_body .= '<DONEE_PHONE></DONEE_PHONE>';
// $soap_body .= '<DONEE_FAX></DONEE_FAX>';
// $soap_body .= '<DONEE_EMAIL></DONEE_EMAIL>';
// $soap_body .= '<CREATE_PROFILE>0</CREATE_PROFILE>';
// $soap_body .= '<USERNAME></USERNAME>';
// $soap_body .= '<PASSWORD></PASSWORD>';
// $soap_body .= '<EXT_CUST_ID></EXT_CUST_ID>';
// $soap_body .= '<DONEE_EXT_CUST_ID></DONEE_EXT_CUST_ID>';
// $soap_body .= '</NewOrderValidate>';
// $soap_body .= '</soap:Body>';
//
$body = '<?xml version="1.0" encoding="utf-8" ?>';
// $body .= '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">';
// $body .= $soap_header;
// $body .= $soap_body;
// $body .= '</soap:Envelope>';
//
//  // remove white space from XML
//  $body = str_replace( array( '\n', '\r', '\t'), '', $body);
